function [ h cp tiss edge ] = dina(genes,gene_map,mtxFolder)
%DINA differential network analysis using and entropy like measure.
%
% INPUT
% genes --> list of gene of interest (official GENE SYMBOLS)
% gene_map --> gene on the rows of the adjacency matrices of the networks
% mtxFolder --> folder in which there are the adjacency matrices
%
% OUTPUT
% h --> entropy
% cp --> vector of co-regulation probability estimated
% tiss --> tissues
% edge --> vector of edges among the genes for each network
%
% EXAMPLE
% genes = [ {'SHMT'} {'SHMT1'} {'PSPH'} {'PSAT1'} {'PHGDH'} {'GLYCTK'} {'PGAM1'} {'PGAM2'} {'PGAM4'} {'BPGM'} {'GRHPR'} {'GCAT'} {'ALAS1'} {'ALAS2'} ]
% gene_map = load('gene_map');
% gene_map = gene_map.gene_map;
% [ h cp tiss edge ] = dina(genes,gene_map,'./mtx/')
%
                     
    id = ismember(gene_map,genes);
        
    files = dir(mtxFolder);
        
    edge = [];
    tiss = [];
    
    %COMPUTES THE EDGE OF THE PATWAYS IN THE N NETWORKS
    for k=3:length(files)
         pathM = strcat(mtxFolder,files(k).name);
                                    
         M = load(pathM);
         M = tril(M.M,-1) + tril(M.M,-1)';
            
         edge = cat(2,edge,length(find(tril(M(id,id)))));         
         tiss = cat(2,tiss,{files(k).name(1:length(files(k).name)-4)});
    end
        
                
     
        
    %ADD PSEUDO COUNT TO EDGES
    edge = edge + 1;
        
    %CO-REGULATION PROBABILITY
    cp = edge./sum(edge);
        
    %ENTROPY
    h = -cp*log2(cp');
                            
    
end

